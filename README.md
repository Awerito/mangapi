# MangAPI

Manga web scrapper with lazy requests and storage on mongodb.

## Setup


Running with docker-compose (after editing user credentials):

```sh
$ docker-compose up -d
```
