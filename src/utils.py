from string import punctuation


def clean_name(name):
    translation_table = dict.fromkeys(map(ord, punctuation), None)
    return name.translate(translation_table)
