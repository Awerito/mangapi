from pydantic import BaseModel, Field


class Manga(BaseModel):
    _id: str
    title: str
    url: str
    image: str
    status: str | None = None


class Chapter(BaseModel):
    id: str = Field(alias="_id")
    name: str
    url: str
    pages: dict[str, str] | None = None


class Pages(BaseModel):
    id: str = Field(alias="_id")
    name: str
    url: str
    pages: dict[str, str]


class Chapters(BaseModel):
    chapters: list[Chapter]
