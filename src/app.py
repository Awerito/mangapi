import httpx
from os import getenv
from dotenv import load_dotenv
from fastapi import FastAPI, HTTPException
from pymongo import MongoClient, UpdateOne
from pymongo.errors import DuplicateKeyError
from fastapi.middleware.cors import CORSMiddleware

from src.utils import clean_name
from src.scraper import mangainn
from src.schemas import Manga, Chapters, Pages

load_dotenv()

app = FastAPI(
    title="MangAPI",
    description="Manga web scrapper with lazy requests and storage on mongodb",
    openapi_url="/mangapi.json",
)

# origins = []

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

db = MongoClient(getenv("MONGODB_URI", "mongodb://localhost:27017"))
doc = db["manga"]
manga_coll = doc["manga"]
chapt_coll = doc["chapter"]
manga_coll.create_index([("title", "text")], name="title")


@app.get("/manga/mangainn/", response_model=Manga, tags=["manga"])
async def manga_search(manga: str):
    if find := manga_coll.find_one({"$text": {"$search": manga.lower()}}):
        del find["_id"]
        return find

    result = mangainn.search_manga(manga)
    if not result:
        raise HTTPException(status_code=404, detail="No manga found")

    name = clean_name(result["title"]).replace(" ", "-").lower()
    result["_id"] = ":".join(["mangainn", name])
    try:
        manga_coll.insert_one(result)
    except DuplicateKeyError:
        pass
    del result["_id"]

    return result


@app.get("/chapters/mangainn/", response_model=Chapters, tags=["chapters"])
async def chapters_search(manga: str):
    find = manga_coll.find_one({"$text": {"$search": manga.lower()}})
    if not find:
        raise HTTPException(status_code=404, detail="No manga found")

    if "status" in find:
        if find["status"] == "Completed":
            chapters = list(chapt_coll.find({"_id": {"$regex": f"^{find['_id']}:"}}))
            if chapters:
                return {"chapters": chapters}

    result, status = mangainn.list_chapters(find["url"])

    find["status"] = status
    manga_coll.update_one({"_id": find["_id"]}, {"$set": find})
    if not result:
        raise HTTPException(status_code=404, detail="No chapters found")

    result = [
        {"_id": f"{find['_id']}:{entry['url'].split('/')[-1]}", **entry}
        for entry in result
    ]

    chapt_coll.bulk_write(
        [UpdateOne({"_id": entry["_id"]}, {"$set": entry}, True) for entry in result]
    )

    return {"chapters": result}


@app.get("/pages/mangainn/", response_model=Pages, tags=["chapters"])
async def pages_of_chapters(manga: str, chapter: float):
    manga_entry = manga_coll.find_one({"$text": {"$search": manga.lower()}})
    if not manga_entry:
        raise HTTPException(status_code=404, detail="No manga found")

    number = f"{chapter:.0f}"
    if chapter - int(chapter) > 0:
        number = f"{chapter:.1f}"

    id = f"{manga_entry['_id']}:{number}"
    chapt_entry = chapt_coll.find_one({"_id": id})
    if not chapt_entry:
        raise HTTPException(status_code=404, detail="No chapter found")

    if "pages" in chapt_entry:
        return chapt_entry

    pages = mangainn.get_pages(chapt_entry)
    chapt_entry = {**chapt_entry, "pages": pages}
    chapt_coll.update_one({"_id": id}, {"$set": chapt_entry})

    return chapt_entry
