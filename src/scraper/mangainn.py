import re
import httpx

from json import loads


def search_manga(manga):
    url = "https://www.mangainn.net/service/search"

    headers = {
        "authority": "www.mangainn.net",
        "accept": "application/json, text/javascript, */*; q=0.01",
        "accept-language": "en-US,en;q=0.9,es;q=0.8",
        "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        "origin": "https://www.mangainn.net",
        "referer": "https://www.mangainn.net/",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-origin",
        "sec-gpc": "1",
        "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36",
        "x-requested-with": "XMLHttpRequest",
    }

    data = f"dataType=json&phrase={manga.lower().replace(' ', '+')}"

    resp = httpx.post(url, headers=headers, data=data)
    if (code := resp.status_code) != 200:
        return None

    result = resp.json()
    if not result:
        return None

    result = result[0]
    del result["tokens"]
    del result["alternative_title"]

    return result


def list_chapters(url):
    headers = {
        "authority": "www.mangainn.net",
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "accept-language": "en-US,en;q=0.9,es;q=0.8",
        "cache-control": "max-age=0",
        "cookie": "ci_session=AW9SOFNjVm1dLAR0VDlQZlU0VmgBcl5zXmIHdA5zVW0Ab1A%2FB1pTaFoxXC5Tbl0vDDwAMF04V2oBcgk5Cj5XO1syADkMMwFoBzIEMVA3BWMBPlJmU2xWb10zBD5UO1BjVWVWZgE2XjZeNQdiDjhVNQBjUGcHNFMzWmRcLlNuXS8MPAAyXTpXagFyCWQKK1dWW2MAZAxmASIHYgR2UCEFdQE1UnFTb1ZkXWEEPVQhUGVVPFZiAWBeOl42BzIOMFU%2BADFQZQc2UztaM1xvU2VdZgw3ADFdOFdgAWoJbwpsVz5bNQA6DGMBMQdkBDFQaAUxATtSNFNmVnVdbAR0VDlQZlU0VmgBcl51XnQHYg5yVVsAYVA2B2BTb1ohXC5Tbl0vDDwAMl06V2YBagkvChZXZlt4AGkMbgE8B2YEKlBnBXkBPlIiU31WD11mBDZUOFB3VUhWOwE%2BXnVefwcnDnhVPAA2UA4HM1M1WnxcLFMUXSwMdgBvXW9XBwE1CW8KEFdgW3YALww3AWMHMAQrUGEFYQEuUipTHlYfXQMESlRPUHtVJFY%2BATlea15iBycOR1VhAGNQOgdqUyhadVxPUz1dLgxpAG5db1d%2FAWEJPQprVydbMgAuDDYBaAc%2BBDNQfAVmATxSNVN1VgRdNgRhVGJQJVVtVn0BZV4zXjAHKQ4zVTIAIlBqB3ZTO1pkXD9Tb11%2BDGoAYl15VyQBDwlsCjhXfVtrAHYMawEkB34EJ1BpBT4BNFIzU2NWYl1mBD5UNlBgVT1WYwFjXjtedAc9DjlVPgAiUCQHdlNkWidcU1MxXT0McgBiXShXawEjCTcKa1czWyAAIgw5ASMHPQQ0UGoFbQEsUm9TNFYjXT8EZFRiUCdVcFYxATheYV5YB2YOblV3AHdQNAd3UyNablxlU29dbgw9AH4%3D; view_video=a%3A13%3A%7Bi%3A0%3Bs%3A4%3A%225359%22%3Bi%3A1%3Bs%3A9%3A%225359-1042%22%3Bi%3A2%3Bs%3A9%3A%225359-1043%22%3Bi%3A3%3Bs%3A6%3A%225359-0%22%3Bi%3A4%3Bs%3A5%3A%2215695%22%3Bi%3A5%3Bs%3A12%3A%225359-782.005%22%3Bi%3A6%3Bs%3A10%3A%225359-782.5%22%3Bi%3A7%3Bs%3A9%3A%225359-1044%22%3Bi%3A8%3Bs%3A9%3A%225359-1046%22%3Bi%3A9%3Bs%3A11%3A%225359-1046.5%22%3Bi%3A10%3Bs%3A9%3A%2215695-104%22%3Bi%3A11%3Bs%3A4%3A%226476%22%3Bi%3A12%3Bs%3A9%3A%225359-1048%22%3B%7D",
        "sec-fetch-dest": "document",
        "sec-fetch-mode": "navigate",
        "sec-fetch-site": "none",
        "sec-fetch-user": "?1",
        "sec-gpc": "1",
        "upgrade-insecure-requests": "1",
        "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36",
    }

    req = httpx.get(url, headers=headers)
    if req.status_code != 200:
        return None

    list_regex = f'<li>\s<a href="(https:\/\/www\.mangainn\.net\/{url.split("/")[-1]}\/.*)">\s<span class="val">(.*) <\/span>'
    status_regex = f"<dt>Status:<\/dt>\s<dd>(.*?)<\/dd>"
    results = re.findall(list_regex, req.text)
    status = re.findall(status_regex, req.text)

    if status:
        status = status[0]
        status = status if status != "-" else "Ongoing"
    else:
        status = "Ongoing"

    if not results:
        return None, status

    data = [{"name": entry[1], "url": entry[0]} for entry in results]
    return data, status


def get_pages(chapter):
    regex = r"var images = (\[.*\])"

    req = httpx.get(chapter["url"])
    if req.status_code != 200:
        return None

    result = re.findall(regex, req.text)
    if not result:
        return None

    pages = loads(result[0])
    return {str(entry["id"]): entry["url"].replace("\\", "") for entry in pages}
